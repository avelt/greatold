# How to use GREAT (GRape Expression ATlas) ?

## First possibility : Access the GREAT shiny app via our website

Make a request for username and password at amandine.velt@inra.fr

Then, go to the GREAT website, use your id and password and access to GREAT application : http://great.colmar.inra.fr/

## Second possibility : Install the GREAT shiny app as an R package (Windows/Linux/Mac) : 

/!\ Install XQuartz on Mac before launching GREAT app, because GREAT app needs X11 /!\

R version >= 3.5.0

### Install following packages

install.packages("shiny")

install.packages("devtools")

install.packages("https://gitlab.com/avelt/great/raw/master/great_0.1.0.tar.gz", repos = NULL, type="source")

### Launch the GREAT shiny app :

library("great")

great::shiny_greatshinyapp()

This command check and install all dependencies packages of GREAT, so the first run can be long depending on the number of packages to install and you internet connexion. 

In all cases, a web browser opens and when the GREAT application appears (not the white screen but buttons, blues borders etc), you can use GREAT app. At the second run, it is very more fast because all the packages were installed.
