observeEvent(input$show_great_database, {

myModal <- function() {
  div(id = "test",
      modalDialog(downloadButton("download_greatdatabase","Download the entire GREAT database"),
                  br(),
                  br(),
                  easyClose = TRUE, title = "Download Table")
  )
} 

  observeEvent(input$test, {
    print("hello")
    showModal(myModal())
  })

  output$my_table_info <- renderDT(
    datatable(info_data_table,
              extensions = 'Buttons',
              options = list(
                dom = 'Bfrtip',
                pageLength = 500,
                buttons = list(  
                  list(extend = "csv",
		  text = "Download filtered GREAT database",
		  filename = paste("GREAT_database_filtered_", Sys.Date(), ".csv", sep="")),
                  list(
                    extend = "collection",
                    text = 'Download entire GREAT database',
                    action = DT::JS("function ( e, dt, node, config ) {
                                    Shiny.setInputValue('test', true, {priority: 'event'});
                    }")
                  )
                )
              )
    )
  )

output$download_greatdatabase <- downloadHandler(
    filename = function() {
      paste("GREAT_database_", Sys.Date(), ".csv", sep="")
    },
    content = function(file) {
      write.csv(info_data_table, file)
    }
  )

})
