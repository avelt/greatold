R
files=list.files(".", pattern="rds")
output <- data.frame(matrix(ncol=3, nrow=length(files)))
colnames(output)=c("comparison", "nb genes up-regulated", "nb genes down-regulated")
i=1
for (file in files){
data=readRDS(file)
comparison=gsub(".txt.rds","",file)
up=dim(data[ which(data$Significance == 1),])[1]
down=dim(data[ which(data$Significance == -1),])[1]
output[i,]=c(comparison,up,down)
i=i+1
}
write.table(output, "resume_table.txt", sep="\t",quote=FALSE)

